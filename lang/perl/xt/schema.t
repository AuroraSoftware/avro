# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

use strict;
use warnings;

use Test::More;
use File::Find;
use_ok 'Avro::Schema';

sub parse {
    next unless /\.avsc$/;
    open(my $fh, '<', $_);
    local $/ = undef;
    my $schema = <$fh>;
    close $fh;
    $schema = Avro::Schema->parse($schema);

    #Todo: Find better location for the following tests
    if(/UnionOfRecordTypes/) {
        use Avro::BinaryEncoder;
        my $output;

        my $data = { 
            'id' => 100,
            'name' => 'Test Example 1',
            'union' => [
                {
                    'org.apache.avro.CustomerServiceTeam' => {
                        'emailAddress' => '',
                        'telephoneNumber' => '',
                        'name' => 'Small Business Customer Services'
                    }
                }
            ]
        };

        Avro::BinaryEncoder->encode(
            schema  => $schema,
            data    => $data,
            emit_cb => sub { $output .= ${ $_[0] } },
        );

        use Avro::BinaryDecoder;
        use IO::String;

        my $decoder = Avro::BinaryDecoder->decode(
            writer_schema => $schema,
            reader_schema => $schema,
            reader => IO::String->new($output),
        );
        is_deeply $decoder, $data;

    }
    note("Successfully parsed: $_");
}

# Ensure that all schema files under the "share" directory can be parsed
{
    find(\&parse, '../../share');
}

done_testing;
